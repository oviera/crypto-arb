#!/usr/bin/env python
# encoding: utf-8
"""
markets_db.py
"""

import sys
import os
import unittest
import sqlite3
import datetime
import time
import csv


class markets_db:
    conn = ''
    
    def __init__(self):
        self.conn = sqlite3.connect('markets.db')
#        conn = sqlite3.connection(":memory:")
        pass
    
    def createDatabase(self):
        try:
            cur = self.conn.cursor()
            cur.execute('''CREATE TABLE tickers (
            timestamp INTEGER ,
            currency TEXT,
            sell FLOAT,
            buy FLOAT,
            updated INTEGER,
            last FLOAT,
            volume FLOAT,
            volume_currency FLOAT,
            high FLOAT,
            low FLOAT,
            average FLOAT,
            servertime INTEGER)''')
            
            self.conn.commit()
        except:
            print "Table already created!"

    def insertIntoTickers(self,req):
        cur = self.conn.cursor()
        timestmp = "'" + str(time.mktime(datetime.datetime.now().timetuple())) + "',"
        str_req = timestmp + req
        
        cur.execute("INSERT INTO tickers VALUES (%s)" % str_req)
        self.conn.commit()
    
    def closeConnection(self):
        self.conn.close()
        
    def writeToCsv(self):
        try:
            cur = self.conn.cursor()
#            cur.execute("select * from tickers;")
            
            cur.execute("select timestamp, buy, high, low, last, volume, last from tickers;")
#        Date,Open,High,Low,Close,Volume,Adj Close
            csv_writer = csv.writer(open("out.csv", 'wt'))
            csv_writer.writerow([i[0] for i in cur.description])
            csv_writer.writerows(cur)
            del csv_writer
        except:
            print "Error!"
                
class markets_dbTests(unittest.TestCase):
    def setUp(self):
        pass


if __name__ == '__main__':
    unittest.main()