import urllib2
import json

class fee:
    def __init__(self):
        pass
      
    def check(self):
      li=['btc_usd', 'btc_rur', 'btc_eur',
	  'ltc_btc', 'ltc_usd', 'ltc_rur',
	  'nmc_btc', 'usd_rur', 'eur_usd', 
	  'nvc_btc', 'trc_btc', 'ppc_btc',
	  'ftc_btc']
      dico={}
      for item in li:
	req = urllib2.Request('https://btc-e.com/api/2/'+item+'/fee')
	response = urllib2.urlopen(req)
	the_page = response.read()
	obj = json.loads(the_page)
	dico[item] = obj['trade']
      return dico
      
 