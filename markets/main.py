import time
import signal
import sys

from threading import Thread

from fee import fee
from ticker import ticker
from trades import trades
from depth import depth
from ticker_factory import ticker_factory
from markets_db import markets_db

class worker(Thread):
    
    Running = True
    
    def __init__(self):
        super(worker, self).__init__()
        
        # init the ticker manager
        self.___ticker = ticker()
        # init the fees grabber
        self.___fee = fee()
        # init the trades grabber
        self.___trades = trades()
        # init the trades grabber
        self.___depth = depth()

    
    def run(self):
        # Main loop !
        db = markets_db()
        db.createDatabase()
        while self.Running:
            tickerdico = self.___ticker.check()
            feedico = self.___fee.check()
            tradesdico = self.___trades.check()
            depthdico = self.___depth.check()
    #       print tickerdico
    #       print feedico
    #       print tradesdico
    #       print depthdico
            time.sleep(1)
            for item in tickerdico:
                ticker_fact = ticker_factory(tickerdico[item],item)
                req_str = ticker_fact.return_sql_insert_values()    
                db.insertIntoTickers(req_str)
        
        db.writeToCsv()
        db.closeConnection()
        print "Closing connection..."
        


worker_thread = worker()

def signal_handler(signal, frame):
    worker_thread.Running = False
    print 'Quitting...'
    sys.exit(0)

def main():
    signal.signal(signal.SIGINT, signal_handler)
   # worker_thread = worker()
    worker_thread.start()
    signal.pause()

if __name__ == "__main__":
    main()
