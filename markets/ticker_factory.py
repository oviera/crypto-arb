#!/usr/bin/env python
# encoding: utf-8

"""
ticker_factory.py
"""

import sys
import os
import unittest


class ticker_factory:
	market_dictionary = ()
	currency = None
	
	def __init__(self,dico,currency):
		self.market_dictionary = dico
		self.currency = currency

	def get_Currency(self):
	    return self.currency
		
	def get_Sell(self):
		return self.market_dictionary['sell']
		
	def get_Buy(self):
		return self.market_dictionary['buy']
		
	def get_Updated(self):
		return self.market_dictionary['updated']
		
	def get_Last(self):
		return self.market_dictionary['last']
		
	def get_Volume(self):
		return self.market_dictionary['vol']
		
	def get_Volume_currency(self):
		return self.market_dictionary['vol_cur']
		
	def get_High(self):
		return self.market_dictionary['high']
		
	def get_Low(self):
		return self.market_dictionary['low']

	def get_Average(self):
		return self.market_dictionary['avg']
		
	def get_ServerTime(self):
		return self.market_dictionary['server_time']
		
	def return_sql_insert_values(self):
		sql_str = "'" + str(self.get_Currency())+"','"+str(self.get_Sell())+"','"+str(self.get_Buy())+"','"+str(self.get_Updated())+"','"+str(self.get_Last())+"','"+str(self.get_Volume())+"','"+str(self.get_Volume_currency())+"','"+str(self.get_High())+"','"+str(self.get_Low())+"','"+str(self.get_Average())+"','"+str(self.get_ServerTime())+"'"
		return sql_str;
		
class ticker_factoryTests(unittest.TestCase):
	def setUp(self):
		pass


if __name__ == '__main__':
	unittest.main()